"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
syntax enable
set background=dark
colorscheme solarized

" Remapped escape will try to move to the left
" prevent this with this mapping
inoremap <Esc> <Esc>`^
