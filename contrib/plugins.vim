" Plugins Configuration
" Decouple plugins from basic configuration
"""""""""""""""""""""""""""""""

""""""""""""""""""""""""""""""
" => Load local plugin configuration files 
""""""""""""""""""""""""""""""
if isdirectory(expand("$HOME/.vim_local/plugin_config"))
  for fpath in split(globpath(expand("$HOME/.vim_local/plugin_config"), '*.vim'), '\n')
    exe 'source' fpath
  endfor
endif
