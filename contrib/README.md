Copy this folder to ~/.vim_local

Move .vimrc to ~/.vimrc

Install your favorite plugins into ~/.vim_local/plugins

Add custom configure options for plugins into separate files in ~/.vim_local/plugin_config

Add custom key mapping options into one or more  files in ~/.vim_local/mapping_config

Add custom file type options into one or more  files in ~/.vim_local/filetype_config

Add custom functions into one or more files in ~/.vim_local/functions
