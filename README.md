# Vim Configuration Foundation
###### Inspired by amix/vimrc

Fork of amix's Ultimate vimrc.

I wanted sane defaults for the most common vim options available as a git repository, but without anything more than that, no extras, no plugins, anything that is a matter of taste or preference I've tried to leave out (at least to a point).  The goal is to include options that are either not really a user preference or so commonly accepted as the best option that it would likely eventually be set to the same value by nearly every user.  Of course, that doesn't mean you can't customize any further, it just means that further customization should be in your own git repository for your home directory files (dot files), this allows a decoupling of plugins from basic vim configuration while maintaining a certain level of consistency across vim installations on multiple computers.

Another goal was to be able to intuitively know where an option was defined without the need of searching through files.  I think a lot of this normally comes from being familiar with the files but I'd like to standardize where and how options are loaded so it is a matter of convention and I'll know exactly where to look for a configuration option. 

For now, the best way to do this is to:

1. Clone to your home directory as .vim
2. Copy the ~/.vim/contrib directory to ~/.vim_local (works especially well if you manage your dot files in vim)
3. Copy ~/.vim_local/_vimrc to ~/.vimrc
4. Add plugins  to the ~/.vim_local/plugins folder (works well if plugins are installed as submodules of your dot file git repository)
5. Add plugin configuration files to ~/.vim_local/plugin_config 

I intend to add some shell scripts later to automate some of the suggested processes

I'm doing this basically to learn and to get everything just the way I want it.  In other words, I'm using amix's excellent work as a starting point for my own vim ennvironment.

By the way, this is a development branch so don't expect anything to work or be anywhere close to finished here. I'll merge to the master branch whenever I feel like it works well enough for others to use.
