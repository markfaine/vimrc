"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General Functions 
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" From an idea by Michael Naumann
" Used by mapping # and *
function! VisualSelection(direction) range
let l:saved_reg = @"
execute "normal! vgvy"

let l:pattern = escape(@", '\\/.*$^~[]')
let l:pattern = substitute(l:pattern, "\n$", "", "")

if a:direction == 'b'
execute "normal ?" . l:pattern . "^M"
elseif a:direction == 'gv'
call CmdLine("vimgrep " . '/'. l:pattern . '/' . ' **/*.')
                                                       elseif a:direction == 'replace'
                                                       call CmdLine("%s" . '/'. l:pattern . '/')
                                                       elseif a:direction == 'f'
                                                       execute "normal /" . l:pattern . "^M"
                                                       endif

                                                       let @/ = l:pattern
                                                       let @" = l:saved_reg
                                                       endfunction

                                                       " Returns true if paste mode is enabled
                                                       function! HasPaste()
                                                       if &paste
                                                       return 'PASTE MODE  '
                                                       endif
                                                       return ''
                                                       endfunction

                                                       " Don't close window, when deleting a buffer
                                                       command! Bclose call <SID>BufcloseCloseIt()
                                                       function! <SID>BufcloseCloseIt()
                                                       let l:currentBufNum = bufnr("%")
                                                       let l:alternateBufNum = bufnr("#")

                                                       if buflisted(l:alternateBufNum)
                                                       buffer #
                                                       else
                                                       bnext
                                                       endif

                                                       if bufnr("%") == l:currentBufNum
                                                       new
                                                       endif

                                                       if buflisted(l:currentBufNum)
                                                       execute("bdelete! ".l:currentBufNum)
                                                       endif
                                                       endfunction

                                                       function! DeleteTillSlash()
                                                       let g:cmd = getcmdline()

                                                       if has("win16") || has("win32")
                                                       let g:cmd_edited = substitute(g:cmd, "\\(.*\[\\\\]\\).*", "\\1", "")
                                                       else
                                                       let g:cmd_edited = substitute(g:cmd, "\\(.*\[/\]\\).*", "\\1", "")
                                                       endif

                                                       if g:cmd == g:cmd_edited
                                                       if has("win16") || has("win32")
                                                       let g:cmd_edited = substitute(g:cmd, "\\(.*\[\\\\\]\\).*\[\\\\\]", "\\1", "")
                                                       else
                                                       let g:cmd_edited = substitute(g:cmd, "\\(.*\[/\]\\).*/", "\\1", "")
endif
endif   

return g:cmd_edited
endfunction

function! CurrentFileDir(cmd)
  return a:cmd . " " . expand("%:p:h") . "/"
  endfunction

  " Delete trailing white space on save, useful for Python and CoffeeScript ;)
function! DeleteTrailingWS()
  exe "normal mz"
  %s/\s\+$//ge
  exe "normal `z"
  endfunction

  " Restore cursor position, window position, and last search after running a
  " command.
function! Preserve(command)
  " Save the last search.
  let search = @/

  " Save the current cursor position.
  let cursor_position = getpos('.')

  " Save the current window position.
  normal! H
  let window_position = getpos('.')
  call setpos('.', cursor_position)

  " Execute the command.
  execute a:command

  " Restore the last search.
  let @/ = search

  " Restore the previous window position.
  call setpos('.', window_position)
  normal! zt

  " Restore the previous cursor position.
  call setpos('.', cursor_position)
  endfunction

  " Re-indent the whole buffer.
function! Indent()
  call Preserve('normal gg=G')
  endfunction
