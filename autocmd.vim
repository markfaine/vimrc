" General autocmd calls
""""""""""""""""""""""""""""""""""""""""""""
" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif

" Remember info about open buffers on close
set viminfo^=%

" When .vimrc is edited, reload it
autocmd! bufwritepost vimrc source ~/.vimrc

" Most accurate but slowest syntax highlighting
autocmd BufEnter * :syntax sync fromstart


